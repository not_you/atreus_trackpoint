#include QMK_KEYBOARD_H
#ifdef RGBLIGHT_ENABLE
#include "rgblight.h"
#endif

/*
const uint8_t RGBLED_RAINBOW_MOOD_INTERVALS[] PROGMEM = {30, 15, 7};
const uint8_t RGBLED_RAINBOW_SWIRL_INTERVALS[] PROGMEM = {25, 13, 5};
*/

#define _LAYER0 0
#define _LAYER1 1
#define _LAYER2 2

enum custom_keycodes {
    LAYER0 = SAFE_RANGE,
    LAYER1,
    LAYER2
};

 const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_LAYER0] = LAYOUT(KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_BTN1, KC_BTN2, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_ESC, KC_TAB, KC_LGUI, KC_LSFT, KC_SPC, TT(2), TT(1), LCTL_T(KC_BSPC), KC_LALT, KC_MINS, KC_QUOT, KC_ENT),

[_LAYER1] = LAYOUT(KC_PSLS, KC_7, KC_8, KC_9, KC_PAST, KC_LCBR, KC_LBRC, KC_RBRC, KC_RCBR, KC_AT, KC_PMNS, KC_4, KC_5, KC_6, KC_PLUS, KC_LEFT, KC_DOWN, KC_UP, KC_RGHT, KC_PIPE, KC_DOT, KC_1, KC_2, KC_3, KC_EQL, KC_BTN1, KC_BTN2, KC_LPRN, KC_RPRN, KC_TILD, KC_AMPR, KC_HASH, KC_ESC, KC_TAB, KC_LGUI, LSFT_T(KC_0), KC_DEL, KC_BTN3, KC_TRNS, LCTL_T(KC_BSPC), LALT_T(KC_BSLS), KC_PERC, KC_EXLM, KC_ENT),

[_LAYER2] = LAYOUT(KC_PSCR, KC_F7, KC_F8, KC_F9, KC_F10, TO(5), KC_MPRV, KC_MPLY, KC_MNXT, TG(3), KC_BRIU, KC_F4, KC_F5, KC_F6, KC_F11, KC_VOLU, KC_VOLD, RGB_VAI, RGB_VAD, KC_WH_U, KC_BRID, KC_F1, KC_F2, KC_F3, KC_F12, KC_BTN1, KC_BTN2, KC_PGUP, KC_END, KC_HOME, KC_PGDN, KC_WH_D, KC_ESC, KC_TAB, KC_LGUI, KC_LSFT, KC_SPC, TT(2), KC_BTN3, KC_LCTL, KC_LALT, KC_BRIU, KC_BRID, KC_ENT),

[_LAYER3] = LAYOUT(KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_NO, KC_NO, KC_NO, KC_NO, TO(0), KC_ESC, KC_A, KC_S, KC_D, KC_G, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LCTL, KC_Z, KC_X, KC_C, KC_F, KC_BTN1, KC_BTN2, KC_NO, KC_NO, KC_NO, KC_NO, KC_NO, KC_LSFT, KC_I, KC_V, KC_M, KC_SPC, KC_SPC, KC_NO, KC_ENT, KC_NO, KC_NO, KC_NO, KC_NO),

};

/*
void keyboard_post_init_user(void) {
  rgblight_enable();
   rgblight_increase_sat();
   rgblight_increase_sat();
   rgblight_increase_sat();
   rgblight_increase_sat();
   rgblight_increase_sat();
  rgblight_increase_val();
  rgblight_increase_val();
  rgblight_set_effect_range(0, 5);
    int value=rgblight_get_val();
	rgblight_sethsv_at(140,210,value, 0);
	rgblight_sethsv_at(0,210,value, 1);
	rgblight_sethsv_at(0,0,value, 2);
	rgblight_sethsv_at(0,210,value, 3);
	rgblight_sethsv_at(140,210,value, 4);
	
}

void matrix_scan_user(void) {
  #ifdef RGBLIGHT_ENABLE

  static uint8_t old_layer = 255;
  uint8_t new_layer = biton32(layer_state);

  if (old_layer != new_layer) {
    rgblight_mode(RGBLIGHT_MODE_STATIC_LIGHT);
    int value=rgblight_get_val();
    switch (new_layer) {
      case _LAYER0:
	rgblight_sethsv_at(140,210,value, 0);
	rgblight_sethsv_at(0,210,value, 1);
	rgblight_sethsv_at(0,0,value, 2);
	rgblight_sethsv_at(0,210,value, 3);
	rgblight_sethsv_at(140,210,value, 4);
        break;
      case _LAYER1:
        rgblight_mode(RGBLIGHT_MODE_RAINBOW_MOOD);
        break;
      case _LAYER2:
        rgblight_mode(RGBLIGHT_MODE_RAINBOW_SWIRL);
        break;
      case _LAYER3:
        rgblight_sethsv_range(0,230,value, 0, 5);
	break;
    }

    old_layer = new_layer;
  }

  #endif //RGBLIGHT_ENABLE
}
*/

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  return true;
}
