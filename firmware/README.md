for documentation on intigrating a trackpoint into your config see https://beta.docs.qmk.fm/using-qmk/hardware-features/feature_ps2_mouse
for documentation on intigrating rgbleds see https://beta.docs.qmk.fm/using-qmk/hardware-features/lighting/feature_rgblight

This directory contains mostly the config i use,
if you want to test your trackpoint,
base your config on it or see if you like it and dont have write your own you can copy this into keyboards/keyboardio/atreus/ of your copy of qmk.
flash with
$make keyboardio/atreus:gay:flash
when it whaits for a device, connect your Atreus while holding down the escape key or press the reset button on the back of the pcb.

I cant be bothered to make a nice ascii art out of the layout so if you want to see it just put gay.json into the QMK configurator website (https://config.qmk.fm/#/keyboardio/atreus/LAYOUT).
