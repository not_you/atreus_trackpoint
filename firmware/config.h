/* Copyright (C) 2019, 2020  Keyboard.io, Inc
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "config_common.h"

/* USB Device descriptor parameter */

#define VENDOR_ID       0x1209
#define PRODUCT_ID      0x2303
#define DEVICE_VER      0x0000
#define MANUFACTURER    Keyboardio
#define PRODUCT         Atreus

/* key matrix size */
#define MATRIX_ROWS 4
#define MATRIX_COLS 12

/* define if matrix has ghost */
//#define MATRIX_HAS_GHOST

/* number of backlight levels */
//#define BACKLIGHT_LEVELS 3

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE 5

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE

/*
 * Keyboard Matrix Assignments
 *
 * Change this to how you wired your keyboard
 * COLS: AVR pins used for columns, left to right
 * ROWS: AVR pins used for rows, top to bottom
 * DIODE_DIRECTION: COL2ROW = COL = Anode (+), ROW = Cathode (-, marked on diode)
 *                  ROW2COL = ROW = Anode (+), COL = Cathode (-, marked on diode)
 *
 */

#define MATRIX_ROW_PINS { F6, F5, F4, F1 }
#define MATRIX_COL_PINS { F7, E2, C7, C6, B6, B5, D7, D6, D4, D5, D3, D2 }
#define UNUSED_PINS

/* COL2ROW, ROW2COL*/
#define DIODE_DIRECTION COL2ROW

//havnt figured out how to use PCINT yet so busywait is the only option
#ifdef PS2_USE_BUSYWAIT
#   define PS2_CLOCK_PORT  PORTB
#   define PS2_CLOCK_PIN   PINB
#   define PS2_CLOCK_DDR   DDRB
#   define PS2_CLOCK_BIT   3
#   define PS2_DATA_PORT   PORTB
#   define PS2_DATA_PIN    PINB
#   define PS2_DATA_DDR    DDRB
#   define PS2_DATA_BIT    1
#endif

#define PS2_MOUSE_X_MULTIPLIER 5
#define PS2_MOUSE_Y_MULTIPLIER 5

//my RGBLIGHT configuration if you want to have all RGBLIGHT_ANIMATIONS you need to disable a bunch of debug stuff in rules.mk
/*
#define RGBLED_NUM 5    
#define RGB_DI_PIN B2    
#define RGBLIGHT_ANIMATIONS    
#define RGBLIGHT_HUE_STEP 8    
#define RGBLIGHT_SAT_STEP 50 
*/


/*
 * Feature disable options
 *  These options are also useful to firmware size reduction.
 */

/* disable debug print */
//#define NO_DEBUG

/* disable print */
//#define NO_PRINT

/* disable action features */
//#define NO_ACTION_LAYER
//#define NO_ACTION_TAPPING
//#define NO_ACTION_ONESHOT
//#define NO_ACTION_MACRO
//#define NO_ACTION_FUNCTION
