$fn=50;
led_space=5.9;
top_str=.6;

linear_extrude(height=top_str)
import(file="led_bracket.dxf",layer=1);

translate([0,0,top_str])
linear_extrude(height=led_space)
import(file="led_bracket.dxf",layer=0);
