REQUIREMENTS:

access to a laser cutter

access to a 3D printer

1 trackpoint in more or less this (https://github.com/alonswartz/trackpoint/blob/master/pinouts/28051.jpg) shape but made out of 2 boards.
they can be found in the x22 and x30 series of thinkpad keyboards.

2 4.7k chip resistors

1 100k chip resistor

1 2.2uf smd mlcc

4 12mm M2 screws

2 6-11mm M2 screws

4 M2 nuts

white or sliver filament

2mm tick laserable material

at least 6mm tick material that's softer than aluminium and you can precisely cut.


HOW TO

1. Laser hole_placement_aid.dxf (and led_hole_placement.dxf if you want RGB leds) out of some kind of stiff foil,

2. Laser the parts from mount.dxf out of 2mm thick material.

3. if you want RGB leds print led_bracket.stl with a white or silver filament.

4. Disassemble your Atreus.

5. place the hole_placement_aid.dxf foil on the inside of the mounting and align it with the upper thumb keys.

6. clip 2 switches in the hole_placement_aid.dxf foil.

7. Mark the locations of the holes in the foil with a center punch.

8. Drill holes at the markings mirrored along the y axis with a 2mm drill and the remaining one with a 3mm drill.

9. repeat steps 4. to 6. with the led_hole_placement.dxf sheet and drill 2mm holes at the markings if you want RGB leds

10. solder thin wires to the upper side of the MISO,SCK,GND and VCC pins on the pcb.
Also solder a wire onto the MOSI pin if you want to add one wire rgbleds as well.

11. Route the wires through the 3mm hole on the mounting plate and the layer 0 part of mount.dxf .

12. Desolder the trackpoint from the second board attached to it(it will hereafter be referred to as the controller board).

13. solder the wires to the controller board and attach the SMDs according to the wiering described here (https://github.com/alonswartz/trackpoint)
(use the pinout from here (https://github.com/alonswartz/trackpoint/blob/master/pinouts/28053.jpg),
its not the same board but the pinout is identical) making sure there is only a little slack in the cables to route them past the screw hole in the center of the x axis.
solder the smd components to the underside of the controller board such that they fit into the pocket of the acrylic underneath it.
Attach SCK to data and MISO to clock if you want compatibility with the provided configs.

14. solder wires onto the pads of the controller board previously attached to the trackpoint.

15. If you want to attach leds rout VCC, GND and MOSI through the gap in the back of layer 0 of mount.dxf.


16. Place layer 1 of mount.dxf onto layer 0, surrounding the controller board.

17. Route the trackpoint wires through the gap in layer 1 and arrange them in the order they need to be attached to the trackpoint(the order in which the pads line up with each other).

18. Put layer 2 and the trackpoint on top and screw all layers as well as the trackpoint to nuts on the other side of the mounting plate with even torque.

19. Solder the wires to the trackpoint.

20. place 5 WS2812 leds from a 60leds/M inside the led_bracket.stl print and screw it to the mounting plate.

21. Flash a firmware with the trackpoint enabled and test it(see firmware/)

22. If everything works reassemble your Atreus.

23. If everything still works enjoy your pointing device zen.
